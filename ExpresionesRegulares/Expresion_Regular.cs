﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ExpresionesRegulares
{
    public static class Expresion_Regular
    {
        /// <summary>
        /// Valid if its a text
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool is_text(string text)
        {
            string regular_expresion = "^[a-zA-Z_áéíóúÁÉÍÓÚñÑ  ]*$";
            return validate(regular_expresion, text);
        }
        /// <summary>
        /// valid if its a text and numbers
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool is_text_and_number(string text)
        {
            string regular_expresion = "^[0-9_a-zA-Z_áéíóúÁÉÍÓÚñÑ  ]*$";
            return validate(regular_expresion, text);
        }
        /// <summary>
        /// Removes extra spaces at the beginning, middle and end of a sentence
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string OnlyOneSpace(string text)
        {
            string sentence = Regex.Replace(text, @"\s+", " ");
            return sentence.Trim();
        }
        /// <summary>
        /// valid if its an email 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool is_email(string text)
        {
            string regularExpresion = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*"+ "@"+ @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";
            return validate(regularExpresion, text);
        }

        private static bool validate(string regular_expresion, string text)
        {
            Regex regex = new Regex(regular_expresion);
            Match match = regex.Match(text);
            if (match.Success)
            {
                return true;
            }
            return false;
        }
    }
}
